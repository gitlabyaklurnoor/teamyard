'use strict'
const path = require('path')

const resolve = dir => {
  return path.join(__dirname, dir)
}

const CompressionPlugin = require("compression-webpack-plugin")

module.exports = {
  // transpileDependencies: ["x5-gmaps"],
  devServer: {
    disableHostCheck: true
  },
  outputDir: "dist",
  runtimeCompiler: true,
  lintOnSave: process.env.NODE_ENV === "development",
  productionSourceMap: false,
  filenameHashing: true,
  chainWebpack: config => {
    config.plugins.delete("prefetch");
    config.plugins.delete("preload");
    config.plugins.delete("preload-index");
    config.plugins.delete("prefetch-index");
    config.plugins.delete("html-subpage");
    config.plugins.delete("preload-subpage");
    config.plugins.delete("prefetch-subpage");
  },
  configureWebpack: {
    devtool: "source-map",
    resolve: {
      alias: {
        "@": resolve("src")
      }
    },
    performance: {
      hints: false
    },
    output: {
      filename: "[name].[hash:8].js",
      chunkFilename: "[name].[hash:8].js"
    },
    plugins: [
      new CompressionPlugin({
        test: /\.js$|\.html$|.\css/,
        threshold: 10240,
        deleteOriginalAssets: false
      })
    ]
  }
};
