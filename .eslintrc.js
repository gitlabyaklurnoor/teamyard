module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  rules: {
    'no-console': 'off',
    // 'no-console': process.env.NODE_ENV !== 'development' ? 'off' : 'error',
    'no-debugger': process.env.NODE_ENV !== 'development' ? 'off' : 'error',
    'no-useless-escape': 0,
    'no-unused-vars': [2, {
      'vars': 'all',
      'args': 'none'
    }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
