const functions = require('firebase-functions');
const admin = require('firebase-admin')
const axios = require('axios').default;
const stripe = require("stripe")(functions.config().stripe.testpublishablekey);
const app = require('./app')

admin.initializeApp();

const db = admin.firestore()

exports.sendConfirmationMail = functions.auth.user().onCreate(async (authUser) => {
    let apiKey = functions.config().sendgrid.apikey;
    // admin.database().ref().update()
    let emailTemplateId = functions.config().sendgrid.emailtemplateid;
    if (authUser.email) {
      try {
        // let customer = await stripe.customers.create({ email: authUser.email });
        // const updates = {};
        // updates[`/customers/${customer.id}`] = authUser.uid;
        // updates[`/user/${authUser.uid}/customerId`] = customer.id;
        // admin.database().update(updates);
        const mail = {
          personalizations: [
            {
              to: [
                {
                  email: authUser.email
                }
              ],
              dynamic_template_data: {
                name: authUser.displayName,
                code: authUser.uid
              },
              subject: `Welcome to Teamyard!`
            }
          ],
          from: {
            email: "turgut@teamyard.co.uk"
          },
          template_id: emailTemplateId
        };
          console.info("mail body", mail);
          const instance = axios.create({
            baseURL: 'https://api.sendgrid.com',
            timeout: 1000,
            headers: {'Authorization': `Bearer ${apiKey}`}
          });

          let response = await instance.post('/v3/mail/send', mail);
           console.info("response", response);
      } catch (error) {
        console.error("error", error);
      }

    }
});

exports.verifyUser = functions.https.onCall(async (data, context) => {
    return new Promise(async (resolve, reject) => {
        try {
            let userId = data.uid;
            let user = await admin.auth().getUser(userId);
          
            let query = await db.collection('company_user').where('userId', '==', userId).limit(1).get();
            const snapshot = query.docs[0]
            let companyUser = snapshot.data();
            let companyId = companyUser.companyId;
            let roles = ["HR_ADMIN"];
            if (!user.emailVerified) {
                await db
                  .collection("company")
                  .doc(companyId)
                  .update({
                    remainMemberSize: admin.firestore.FieldValue.increment(+1)
                  });

                let companySnapshot = await db
                  .collection("company")
                  .doc(companyId)
                  .get();
                let company = companySnapshot.data();
                
                if (company.remainMemberSize > 2) {
                  roles = ["EMPLOYE"];
                }
            }
            
            if(user){
                await admin.auth().setCustomUserClaims(userId, { roles, companyId, completedCompanyInfo: false});
                await admin.auth().updateUser(userId, { emailVerified : true});
                resolve(
                    {
                        code: "00",
                        message: "Response Approved",
                        data: user.toJSON(),
                        status: "APPROVED"
                    }
                );
            }else{
                console.log('user not found');
                reject(new Error("user not found"));
            }
          
        } catch (error) {
          console.log(error);
          reject(new Error(error));
        }
    })
  });


exports.updatePassword = functions.https.onCall(async (data, context) => {
    return new Promise(async (resolve, reject) => {
        try {
            let userId = data.uid;
            let password = data.password;
            let confirmPassword = data.confirmPassword;
            if(password && confirmPassword && password === confirmPassword){
                let user = await admin.auth().getUser(userId);
                if(user){
                    admin.auth().updateUser(userId, { password});
                    resolve(
                        {
                            code: "00",
                            message: "Response Approved",
                            data: user.toJSON(),
                            status: "APPROVED"
                        }
                    );
                }else{
                    console.log('user not found');
                    reject(new Error("user not found"));
                }
            }else{
                console.log("password not matched");
                reject(new Error("password not matched"));
            }
          
        } catch (error) {
            reject(new Error(error));
        }

    });
});

exports.forgetPassword = functions.https.onCall(async (data, context) => {
  return new Promise(async (resolve, reject) => {
    let apiKey = functions.config().sendgrid.apikey;
    let emailTemplateId = functions.config().sendgrid.resetpasswordtemplateid;
    let email = data.email;
    if (email) {
      try {
        let query = await db
          .collection("users")
          .where("email", "==", email)
          .limit(1)
          .get();
        const snapshot = query.docs[0];
        let user = snapshot.data();
        let uuid = user.uuid;
        const mail = {
          personalizations: [
            {
              to: [
                {
                  email: email
                }
              ],
              dynamic_template_data: {
                code: uuid
              },
              subject: `Reset your password!`
            }
          ],
          from: {
            email: "turgut@teamyard.co.uk"
          },
          template_id: emailTemplateId
        };
        console.info("mail body", mail);
        const instance = axios.create({
          baseURL: "https://api.sendgrid.com",
          timeout: 1000,
          headers: { Authorization: `Bearer ${apiKey}` }
        });

        let response = await instance.post("/v3/mail/send", mail);
        console.info("response", response);
      } catch (error) {
        console.error("error", error);
      }
    }
  });
});

exports.recurringPayment = functions.https.onRequest(async (request, response) => {

  const hook = request.body.type;
  const data = request.body.data.object;
  if (!data) throw new Error( "missing data");

 return admin
   .database()
   .ref(`/customers/${data.customer}`)
   .once("value")
   .then(snapshot => snapshot.val())
   .then(userId => {
     const ref = admin.database().ref(`/users/${userId}/pro-member`);
     if (hook === "invoice.payment_succeeded") {
       return ref.update({ status: "active" });
     }

     if (hook === "invoice.payment_failed") {
       return ref.update({ status: "pastDue" });
     }
     throw new Error("not a matching webhook ");
   })
   .then(() => response.status(200).send(`successfully handled ${hook}`))
   .catch(error => response.status(400));
   
});


exports.createSubscription = functions.database.ref(`/users/{userId}/plan/token`)
  .onWrite(event => {
    let tokenId = event.data.val();
    let userId = event.params.userId;

    if (!tokenId) throw new Error(`missin token `);
      
    return  admin
          .database()
          .ref(`/users/${userId}`)
          .once("value")
          .then(snapshot => snapshot.val())
          .then(user => {
            console.log(user);



            return stripe.subscriptions.create({
              customer: user.customerId,
              source: tokenId,
              items: [
                {
                  plan: ""
                }
              ]
            })
          })
          .then(sub => {
            return admin
              .database()
              .ref(`/users/${userId}/pro-member`)
              .update({ status: "active" });
          })
          .catch(err => {
            throw new Error(err);
          });
  });


// exports.google = functions.https.onRequest(app);
exports.google = functions.https.onRequest(async (req, res) => {
    const keyword = req.query.keyword;
    const location = req.query.location;
    functions.logger.info("keyword", keyword);
    functions.logger.info("location", location);
    res.set("Access-Control-Allow-Origin", "*");
    let fullUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=${keyword}&location=${location}&radius=10000&key=AIzaSyAAz6AT8zPEXWe9iL3vKqCgpHv7LCjV8Bo`;
    try {
      let response = await axios.get(fullUrl);
      functions.logger.info(`responseMapPlaceApi`,response);
      res.send(response);
    } catch (error) {
      functions.logger.error(error);
      res.status(400).json({ data: error });
    }
});
// exports.nearbyPlaceList = functions.https.onRequest(async (req, res) => {
//     const keyword = req.query.keyword;
//     const location = req.query.location;
//     res.set("Access-Control-Allow-Origin", "*");
//     let fullUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=${keyword}&location=${location}&radius=10000&key=AIzaSyAAz6AT8zPEXWe9iL3vKqCgpHv7LCjV8Bo`;
//     if (req.method === "OPTIONS") {
//       res.set("Access-Control-Allow-Methods", "GET");
//       res.set("Access-Control-Max-Age", "3600");
//       res.status(204).json("");
//     } else {
//       let response = await axios.get(fullUrl);
//       functions.logger('')
//        console.info("response", response);
//       res.status(200).json({ data: response });
//     }
//   });

exports.createEvent = functions.https.onCall(async (data, context) => {
    return new Promise(async (resolve, reject) => {
            let eventRequest = data.event;
            let apiKey = functions.config().sendgrid.apikey;
            let eventinvationtemplatedid = functions.config().sendgrid
            .eventinvationtemplatedid;
            try {
                    let auth = context.auth;
                    if(auth){
                        let userId = auth.uid;
                        let eventRef = await db
                        .collection("event")
                        .add({
                            ...eventRequest,
                            userId
                        });
                        const eventId = eventRef.id;
                        if(eventId){
                            functions.logger.info(`eventId`,eventId);
                            functions.logger.info(`userId`, userId);
                            let authUser = await admin.auth().getUser(userId);
                            const displayName = authUser.displayName;
                            functions.logger.info(`authUser`, authUser);
                            functions.logger.info(`displayName`, displayName);
                            eventRequest.users.forEach(async user => {
                                functions.logger.info(`user`, user);
                                const mail = {
                                personalizations: [
                                    {
                                    to: [
                                        {
                                            email: user.email
                                        }
                                    ],
                                    dynamic_template_data: {
                                        name: user.firstName,
                                        code: eventId,
                                        userName: displayName
                                    },
                                    subject: `Someone has invited you to join an event.`
                                    }
                                ],
                                from: {
                                    email: "turgut@teamyard.co.uk"
                                },
                                template_id: eventinvationtemplatedid
                                };
                                functions.logger.info(`mail body`, mail);
                                const instance = axios.create({
                                    baseURL: "https://api.sendgrid.com",
                                    timeout: 1000,
                                    headers: { Authorization: `Bearer ${apiKey}` }
                                });
        
                                let message = instance.post("/v3/mail/send", mail);
                                functions.logger.info(`message`, message);
                            });
                            functions.logger.info(`event`, eventRequest);
                            resolve(
                                {
                                    code: "00",
                                    message: "Response Approved",
                                    data: { eventId: eventId},
                                    status: "APPROVED"
                                }
                            );
                        }
                    } else {
                        reject(new Error("auth not found"));
                    }
                    
            } catch (error) {
            console.log(error);
            reject(new Error(error));
            }
    })
});



