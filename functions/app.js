const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors({ origin: true }));

// build multiple CRUD interfaces:
app.get("/map/place", (req, res) => {
  res.send("turgut");
});
// app.post("/", (req, res) => res.send(Widgets.create()));
// app.put("/:id", (req, res) =>
//   res.send(Widgets.update(req.params.id, req.body))
// );
// app.delete("/:id", (req, res) => res.send(Widgets.delete(req.params.id)));
// app.get("/", (req, res) => res.send(Widgets.list()));

exports.app = app;
