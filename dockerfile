FROM node
COPY ./ /app
WORKDIR /app

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

RUN npm install && npm rebuild node-sass && npm run build-${NODE_ENV}

FROM nginx:1.17.8-alpine
RUN mkdir /app
COPY --from=0 /app/dist /app
COPY ./config/nginx.conf /etc/nginx/nginx.conf
