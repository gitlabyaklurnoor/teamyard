export default async (to, from, next) => {
  const onlyAuthUser = to.matched.some(record => record.meta.onlyAuthUser);
  console.log(to.path, onlyAuthUser);
  if (to.path !== '/auth/login') {
     if (onlyAuthUser) {
       let authenticatedUser = JSON.parse(
         localStorage.getItem("authenticatedUser")
       );
       if (!authenticatedUser) {
         return next("/auth/login");
       } else {
         let isCompanyAdded = authenticatedUser.completedCompanyInfo;
        //  let skipPaymentFlow = authenticatedUser.skipPaymentFlow;
         let isPaymentAdded = authenticatedUser.completedPaymentInfo;
         let isProfileCompleted =  authenticatedUser.photoURL;
         let isActivitiesCompleted = authenticatedUser.activities;
         let memberCount = authenticatedUser.memberCount;
         console.log("isCompanyAdded", isCompanyAdded);
         console.log("isProfileCompleted", isProfileCompleted);
         console.log("isActivitiesCompleted", isActivitiesCompleted);

         if (!isProfileCompleted) {
           return next("/profile/update");
         }

         if (!isCompanyAdded && authenticatedUser.roles.includes("HR_ADMIN")) {
           return next("/enterprise/company-create");
         }

         if (
           !isActivitiesCompleted &&
           authenticatedUser.roles.includes("EMPLOYE")
         ) {
           return next("/user/activities");
         }

         if (!isPaymentAdded && authenticatedUser.roles.includes("HR_ADMIN")) {
           return next("/payment");
         }

         if (
           memberCount === 2 &&
           authenticatedUser.roles.includes("HR_ADMIN")
         ) {
           return next("/enterprise/invite/panel");
         }

         return next();
       }
     }
  }
 
};
