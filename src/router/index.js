import Vue from 'vue'
import Router from 'vue-router'
import CheckAuth from './middleware/CheckAuth';
// import firebase from 'firebase';

Vue.use(Router)

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/dashboard",
      name: "Overview",
      component: () => import("../pages/dashboard/Overview.vue"),
      meta: { onlyAuthUser: true, layout: "default" },
      beforeEnter: CheckAuth
    },
    {
      path: "/auth/login",
      name: "Login",
      component: () => import("../pages/auth/Login.vue"),
      meta: { layout: "auth" }
    },
    {
      path: "/forgot-password",
      name: "Forgot Password",
      component: () => import("../pages/auth/ForgotPassword.vue"),
      meta: { layout: "auth" }
    },
    {
      path: "/reset-password-confirmation",
      name: "Reset Password",
      component: () => import("../pages/auth/ResetPassword.vue"),
      meta: { layout: "auth" }
    },
    {
      path: "/register",
      name: "Register",
      component: () => import("../pages/auth/Register.vue"),
      meta: { layout: "auth", onlyAuthUser: false }
    },
    {
      path: "/",
      name: "Home",
      component: () => import("../pages/home/Home.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/what-teamyard",
      name: "Home",
      component: () => import("../pages/home/WhatIsTeamyard.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/contact-us",
      name: "ContactUs",
      component: () => import("../pages/home/ContactUs.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },

    {
      path: "/privacy-terms",
      name: "PrivacyTerms",
      component: () => import("../pages/home/PrivacyTerms.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/cookie-policy",
      name: "CookiePolicy",
      component: () => import("../pages/home/CookiePolicy.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/term-of-use",
      name: "TermOfUse",
      component: () => import("../pages/home/TermOfUse.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },

    {
      path: "/how-it-works",
      name: "Home",
      component: () => import("../pages/home/HowItWorks.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/enterprise",
      name: "Enterprise",
      component: () => import("../pages/home/Enterprise.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/enterprise/signup",
      name: "EnterpriseSignup",
      component: () => import("../pages/home/EnterpriseRegister.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/enterprise/signup-completed",
      name: "EnterpriseSignupCompleted",
      component: () => import("../pages/home/EnterpriseRegisterCompleted.vue"),
      meta: { layout: "info", onlyAuthUser: false }
    },
    {
      path: "/enterprise/set-password",
      name: "EnterpriseSetPassword",
      component: () => import("../pages/auth/ResetPassword.vue"),
      meta: { layout: "auth", onlyAuthUser: false }
    },
    {
      path: "/enterprise/company-create",
      name: "EnterpriseCompanyCreate",
      component: () => import("../pages/company/CreateCompany.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN"]
      }
    },
    {
      path: "/enterprise/invite/panel",
      name: "EnterpriseInvitePanel",
      component: () => import("../pages/company/invite/InvitePanel.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/enterprise/invite",
      name: "EnterpriseInvite",
      component: () => import("../pages/company/invite/CreateInvite.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/profile/update",
      name: "Profile",
      component: () => import("../pages/account-settings/Information.vue"),
      meta: {
        onlyAuthUser: true,
        layout: "default",
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/user/activities",
      name: "EnterpriseInvite",
      component: () => import("../pages/activities/Index.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/user/events",
      name: "UserEvents",
      component: () => import("../pages/events/events.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/user/events/invite/:id",
      name: "UserEventInviteSample",
      component: () => import("../pages/events/EventInviteSample.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/user/events/invite/:id/payment",
      name: "UserEventInviteSample",
      component: () => import("../pages/events/EventInvitePayment.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/user/events-create",
      name: "UserEvents",
      component: () => import("../pages/events/index.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
    },
    {
      path: "/payment",
      name: "Payment",
      component: () => import("../pages/payment/Index.vue"),
      meta: {
        layout: "default",
        onlyAuthUser: true,
        roles: ["HR_ADMIN", "EMPLOYE"]
      }
      // beforeEnter: CheckAuth
    },
    {
      path: "/unauthenticated",
      name: "PageNotAuthenticated",
      component: () => import("../pages/auth/PageNotAuthenticated.vue"),
      meta: { layout: "auth", onlyAuthUser: false }
    },
    {
      path: "/unauthorized",
      name: "PageNotAuthorized",
      component: () => import("../pages/auth/PageNotAuthorized.vue"),
      meta: { layout: "auth", onlyAuthUser: false }
    },
    {
      path: "*",
      name: "PageNotFound",
      component: () => import("../pages/general/PageNotFound.vue"),
      meta: { layout: "auth", onlyAuthUser: false }
    }
  ]
});


router.beforeEach((to, from, next) => {
    // firebase.auth().onAuthStateChanged( userAuth => {
    //     if(userAuth){
    //        let currentUser = firebase.auth().currentUser;
    //        let token = firebase.auth().currentUser.getIdTokenResult;
    //        console.log(token);
    //        console.log(currentUser);
    //     }
    // });
    
    let authenticatedUser = JSON.parse(localStorage.getItem('authenticatedUser'));
  const { roles } = to.meta;
  if (to.matched.some(record => record.meta.onlyAuthUser)) {

      if (authenticatedUser) {
        const userRoles = authenticatedUser.roles;
        console.log(authenticatedUser);
        if(userRoles){
            if(userRoles && roles && roles.length && userRoles.length && !roles.some(r=> userRoles.includes(r))) {
                return next({ path: '/unauthorized' });
            }else{
                return next();
            }
        }else{
          return next({ path: "/unauthorized" }); 
        }
      } else {
        window.location = '/auth/login';
      }
    }
    return next();
  })

export default router
