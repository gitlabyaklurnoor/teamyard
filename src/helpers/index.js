export const rejectError = ({response = null}) => {
    let message = 'The server encountered an error which may have several reasons such as poor connectivity or internal server error.'

    if (response) {
        message = response;
    }
    return Promise.reject(message)
}

export const applyFilters = (url, filter) => {
    if (filter) {
        let filteredEntities = ''
        if (url.indexOf('?') === -1) {
            url += '?'
        } else {
            url += '&'
        }

        Object.keys(filter).forEach(key => {
            filteredEntities += `${key}=${filter[key]}&`;
        })

        if (filteredEntities.slice(-1) === '&') {
            filteredEntities = filteredEntities.slice(0, -1)
        }

        return url + filteredEntities
    }

    return url
}

export const processLocation = location => {
    return location.toLowerCase().replace(/[\s,]+/g,'').trim()
}