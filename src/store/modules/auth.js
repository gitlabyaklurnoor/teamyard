import axiosInstance from '@/services'
import { rejectError } from '@/helpers'
import firebase from 'firebase';

export default {
  namespaced: true,
  state: {
    user: null,
    passwordReset: null,
    isAuthResolved: false,
    currentJWT: localStorage.getItem("merchant-panel-token") || ""
  },
  getters: {
    authUser: (state, getters) =>
      state.currentJWT ? JSON.parse(atob(getters.jwt.split(".")[1])) : null,
    isAuthenticated(state) {
      return !!state.user;
    },
    jwt: state => state.currentJWT,
    getPasswordReset(state) {
      return state.passwordReset;
    }
  },
  actions: {
    loginWithEmailAndPassword({ commit }, userData) {
      return new Promise(async (resolve, reject) => {
        try {
          const { email, password } = userData;
          const { user } = await firebase
            .auth()
            .signInWithEmailAndPassword(email, password);
          let tokenResult = await firebase
            .auth()
            .currentUser.getIdTokenResult();

          let roles = tokenResult.claims.roles;
          let companyId = tokenResult.claims.companyId;
          let companySnapshot = await firebase
            .firestore()
            .collection("company")
            .doc(companyId)
            .get();
          let userInfo = await (
            await firebase
              .firestore()
              .collection("users")
              .where("uuid", "==", user.uid)
              .get()
          ).docs[0].data();
          const companyInfo = companySnapshot.data();
          let completedCompanyInfo = companyInfo.completedCompanyInfo;
          let completedPaymentInfo = companyInfo.completedPaymentInfo;
          let autUser = getUserFromFirebase({
            ...user,
            roles,
            companyId,
            completedCompanyInfo: completedCompanyInfo
              ? completedCompanyInfo
              : false,
            completedAccountInfo: userInfo.jobTitle === "-" ? false : true,
            activities: userInfo.activities,
            userCollectionId: (
              await firebase
                .firestore()
                .collection("users")
                .where("uuid", "==", user.uid)
                .get()
            ).docs[0].id,
            memberCount: companyInfo.remainMemberSize,
            companyLogo: companyInfo.logo,
            completedPaymentInfo: completedPaymentInfo,
            plan: userInfo.plan
          });
          if (autUser && user.emailVerified) {
            localStorage.setItem("authenticatedUser", JSON.stringify(autUser));
            commit("setJWT", autUser.accessToken);
            resolve(autUser);
          } else {
            reject();
          }
        } catch (error) {
          reject(error);
        }
      });
    },
    signUp({ commit }, userData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {
            email,
            firstName,
            lastName,
            allowMarketingAndAnnouncement,
            plan
          } = userData;
          const { user } = await firebase
            .auth()
            .createUserWithEmailAndPassword(email, "password");
          let currentUser = firebase.auth().currentUser;

          let planDetailsnap = await firebase
            .firestore()
            .collection("plans")
            .doc(plan)
            .get();

          let planDetail = planDetailsnap.data();

          let subscription = {
            price: planDetail.price,
            type: planDetail.type,
            stripe_id: planDetail.stripe_id,
            teamSize: planDetail.teamSize
          };
          
          let company = {
            name: "-",
            location: "-",
            phoneNumber: "-",
            website: "-",
            logo: "-",
            remainMemberSize: 1,
            email: currentUser.email,
            teamSize: 25,
            completedCompanyInfo: false,
            completedPaymentInfo: false
          };
          // firebase.auth.RecaptchaVerifier()
          let response = await firebase
            .firestore()
            .collection("company")
            .add({ ...company });
          
          let companyId = response.id;

          if (response.id) {
            let userResponse = await firebase
              .firestore()
              .collection("users")
              .add({
                displayName: `${firstName} ${lastName}`,
                firstName,
                lastName,
                uuid: user.uid,
                phoneNumber: "-",
                allowMarketingAndAnnouncement,
                plan: subscription,
                jobTitle: "-",
                email: currentUser.email
              });

            if (userResponse.id) {
              await currentUser.updateProfile({
                displayName: `${firstName} ${lastName}`
              });
              await firebase
                .firestore()
                .collection("company_user")
                .add({
                  companyId,
                  userId: user.uid
                });
              let autUser = getUserFromFirebase({
                ...user,
                userCollectionId: (
                  await firebase
                    .firestore()
                    .collection("users")
                    .where("uuid", "==", user.uid)
                    .get()
                ).docs[0].id
              });
              if (autUser) {
                resolve("The registration is successfully");
              } else {
                reject("you should comfirm email");
              }
            } else {
              rejectError("there is an unexpected error");
            }
          } else {
            reject("somethings get wrong");
          }
        } catch (error) {
          // rejectError(error)
          reject(error);
        }
      });
    },

    logout({ commit }) {
      return new Promise(async (resolve, reject) => {
        try {
          await firebase.auth().signOut();
          localStorage.clear();
          // commit();
          resolve(null);
        } catch (error) {
          console.log(error);
          rejectError(error);
          reject(error);
        }
      });
    },
    sendPasswordResetLink({ commit }, userData) {
      return new Promise(async (resolve, reject) => {
        try {
          // const { email } = userData;
          // await firebase.auth().sendPasswordResetEmail(email);
          // resolve();
          var addMessage = firebase.functions().httpsCallable("forgetPassword");
          let response = await addMessage({ uid: userData.token });
          resolve(response);
          commit();
        } catch (error) {
          reject(error);
        }
      });
    },
    validatePasswordResetLink({ commit }, resetPasswordData) {
      return new Promise(async (resolve, reject) => {
        try {
          var addMessage = firebase.functions().httpsCallable("verifyUser");
          let response = await addMessage({ uid: resetPasswordData.token });
          resolve(response);
          commit();
        } catch (error) {
          reject(error);
        }
      });
    },
    processPasswordReset({ commit }, userData) {
      return new Promise(async (resolve, reject) => {
        try {
          const { token, password, confirmPassword } = userData;
          var addMessage = firebase.functions().httpsCallable("updatePassword");
          let response = await addMessage({
            uid: token,
            password,
            confirmPassword
          });
          if (response) {
            // commit('setJWT', response.data.token);
            resolve(response);
          } else {
            rejectError("there is an unexpected error");
            reject();
          }
        } catch (error) {
          rejectError(error);
          reject(error);
        }
      });
    },
    validateSelfRegistration({ commit }, data) {
      const token = data.token;
      return axiosInstance
        .post(
          `/self-registration/self-registration-request/validateConfirmationMail/${token}`,
          {}
        )
        .then(res => {
          const response = res.data;
          if (
            response.code === "00" &&
            response.status === "APPROVED" &&
            res.data == true
          ) {
            if (process.env.NODE_ENV === "development") {
              window.console.log(res.data);
            }
            return res.data;
          } else {
            return res.data;
          }
        })
        .catch(err => {
          return rejectError(err);
        });
    },
    getAuthUserDetail({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        try {
          let { uid } = data;
          let query = await firebase
            .firestore()
            .collection("users")
            .where("uuid", "==", uid)
            .get();
          if (!query.empty) {
            const snapshot = query.docs[0];
            const userDetail = snapshot.data();
            resolve(userDetail);
          } else {
            reject("user not found");
          }
        } catch (error) {
          reject(error);
        }
      });
    },
    manuelInvite({ commit }, userData) {
      return new Promise(async (resolve, reject) => {
        try {
          const {
            email,
            firstName,
            lastName,
            jobTitle,
            officeLocation,
            department
          } = userData;
          const { user } = await firebase
            .auth()
            .createUserWithEmailAndPassword(email, "password");
          let currentUser = firebase.auth().currentUser;
          let authenticatedUser = JSON.parse(
            localStorage.getItem("authenticatedUser")
          );

          let companyId = authenticatedUser.companyId;

          let userResponse = await firebase
            .firestore()
            .collection("users")
            .add({
              displayName: `${firstName} ${lastName}`,
              firstName,
              lastName,
              uuid: user.uid,
              phoneNumber: "-",
              allowMarketingAndAnnouncement: true,
              jobTitle,
              officeLocation,
              department,
              email
            });

          if (userResponse.id) {
            await currentUser.updateProfile({
              displayName: `${firstName} ${lastName}`
            });
            let companyUserResponse = await firebase
              .firestore()
              .collection("company_user")
              .add({
                companyId,
                userId: user.uid
              });

            if (companyUserResponse.id) {
              resolve("The Invite sent is successfully");
            } else {
              reject("there is an unexpected error");
            }
          } else {
            rejectError("there is an unexpected error");
          }
        } catch (error) {
          // rejectError(error)
          reject(error);
        }
      });
    },
    contactUs({ commit }, contactData) {
      return new Promise(async (resolve, reject) => {
        try {
          const { email, fullName, subject, message } = contactData;

          let contactResponse = await firebase
            .firestore()
            .collection("contact")
            .add({
              fullName,
              subject,
              message,
              email
            });

            if (contactResponse.id) {
              resolve("The message sent is successfully");
          } else {
            rejectError("there is an unexpected error");
          }
        } catch (error) {
          // rejectError(error)
          reject(error);
        }
      });
    }
  },

  mutations: {
    setAuthUser(state, user) {
      return (state.user = user);
    },
    setAuthState(state, authState) {
      return (state.isAuthResolved = authState);
    },
    setJWT(state, jwt) {
      state.currentJWT = jwt;
    },
    setPasswordResetRequest(state, passwordReset) {
      state.passwordReset = passwordReset;
    }
  }
};

function getUserFromFirebase(response) {
    const accessToken = response.refreshToken;
    let authUser =  response;
    const user = {
      email: authUser.email,
      displayName: authUser.displayName,
      uuid: authUser.uid,
      exp: authUser.metadata.a,
      lastLoginAt: authUser.metadata.b,
      accessToken,
      roles: authUser.roles,
      companyId: authUser.companyId,
      completedCompanyInfo: authUser.completedCompanyInfo,
      photoURL: authUser.photoURL,
      activities: authUser.activities,
      userCollectionId: authUser.userCollectionId,
      memberCount: authUser.memberCount,
      companyLogo: authUser.companyLogo,
      completedPaymentInfo: authUser.completedPaymentInfo,
      plan: authUser.plan
    };

    return user;
}