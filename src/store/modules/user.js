import { rejectError } from "@/helpers";
import axiosInstance from "../../services";
import firebase from "firebase";

export default {
  namespaced: true,
  state: {
    users: [],
    userDetail: {
      userClientRoles: []
    },
    deleteUserResponse: {},
    resetPasswordResponse: {},
    createUser: {},
    updateUser: {},
    activities: []
  },
  getters: {
    users(state) {
      return state.users;
    },
    getUserLogs(state) {
      return state.userLogs;
    },
    getUserDetail(state) {
      if (state.userDetail) {
        return state.userDetail;
      }
      return null;
    },
    getDeleteUser(state) {
      return state.deleteUserResponse;
    },
    getCreateUser(state) {
      return state.createUser;
    },
    getUpdateUser(state) {
      return state.updateUser;
    },
    getResetPassword(state) {
      return state.resetPasswordResponse;
    },
    getActivities(state) {
      if (state.activities) {
        // let userActivities = JSON.parse(localStorage.getItem('authenticatedUser')).activities;
        // let mapped = state.activities.map(k =>  k.pressed = userActivities.include(k.id));
        // console.log(mapped);
        return state.activities;
      }
      console.log("turgut");

      return null;
    }
  },
  actions: {
    async getUsers({ commit }, data) {
      try {
        let user = JSON.parse(localStorage.getItem("authenticatedUser"));
        const res = await firebase
          .firestore()
          .collection("users")
          .where("uuid", "==", user.uuid)
          .get()
          .docs[0].data();
        const response = res.data;
        if (response) {
          if (process.env.NODE_ENV === "development") {
            window.console.log(response);
          }
          commit("setUsers", response);
        } else {
          rejectError("there is an unexpected error");
        }
      } catch (err) {
        return rejectError(err);
      }
    },
    async deleteUser({ commit }, data) {
      const { id } = data;

      try {
        const res = await axiosInstance.delete(
          `/merchants/merchantuser/panel/${id}`
        );
        const response = res.data;

        if (response.code === "00" && response.status === "APPROVED") {
          if (process.env.NODE_ENV === "development") {
            window.console.log(response.data);
          }

          commit("setDeleteUser", response.data);
        } else {
          rejectError("there is an unexpected error");
        }
      } catch (err) {
        return rejectError(err);
      }
    },
    async createUser({ commit }, data) {
      try {
        const res = await axiosInstance.post(
          `/merchants/merchantuser/panel/`,
          data
        );
        const response = res.data;

        if (response.code === "00" && response.status === "APPROVED") {
          if (process.env.NODE_ENV === "development") {
            window.console.log(response.data);
          }

          commit("setCreateUser", response.data);
        } else {
          rejectError("there is an unexpected error");
        }
      } catch (err) {
        return rejectError(err);
      }
    },
    async updateUser({ commit }, data) {
      const { UUID } = data;
      try {
        const res = await axiosInstance.put(
          `/merchants/merchantuser/panel/${UUID}`,
          data
        );
        const response = res.data;

        if (response.code === "00" && response.status === "APPROVED") {
          if (process.env.NODE_ENV === "development") {
            window.console.log(response.data);
          }

          commit("setUpdateUser", response.data);
        } else {
          rejectError("there is an unexpected error");
        }
      } catch (err) {
        return rejectError(err);
      }
    },
    getUserDetail({ commit }, data) {
      // const { userId } = data
      return new Promise(async (resolve, reject) => {
        try {
          let user = JSON.parse(localStorage.getItem("authenticatedUser"));
          const response = (
            await firebase
              .firestore()
              .collection("users")
              .where("uuid", "==", user.uuid)
              .get()
          ).docs[0].data();
          if (response) {
            commit("setUserDetail", { ...response , email: user.email});
            resolve(response);
          } else {
            reject("there is an unexpected error");
          }
        } catch (err) {
          return reject(err);
        }
      });
    },
    async resetPassword({ commit }, data) {
      try {
        const res = await axiosInstance.put(
          `/merchants/merchantuser/panel/reset-password`,
          data
        );
        const response = res.data;

        if (response.code === "00" && response.status === "APPROVED") {
          if (process.env.NODE_ENV === "development") {
            window.console.log(response.data);
          }

          commit("setResetPassword", response.data);
        } else {
          rejectError("there is an unexpected error");
        }
      } catch (err) {
        return rejectError(err);
      }
    },
    getActivities({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        try {
          let userActivities = JSON.parse(
            localStorage.getItem("authenticatedUser")
          ).activities;
          var query = firebase
            .firestore()
            .collection("activities")
            .where("status", "==", true)
            .limit(50);
          const { docs } = await query.get();
          let activities = docs.map(doc => {
            const { id } = doc;
            const data = doc.data();
            return { id, ...data };
          });

          if (activities) {
            activities.map(k => {
              k.pressed = userActivities && userActivities.includes(k.id);
            });

            if (process.env.NODE_ENV === "development") {
              window.console.log(activities);
            }
            commit("setActivities", activities);
            resolve();
          } else {
            reject("there is an unexpected error");
          }
        } catch (err) {
          return reject(err);
        }
      });
    }
  },
  mutations: {
    setUsers(state, users) {
      return (state.users = users);
    },
    setUserDetail(state, response) {
      return (state.userDetail = response);
    },
    setUserLogs(state, response) {
      return (state.userLogs = response);
    },
    setDeleteUser(state, response) {
      return (state.deleteUserResponse = response);
    },
    setCreateUser(state, response) {
      return (state.createUser = response);
    },
    setUpdateUser(state, response) {
      return (state.updateUser = response);
    },
    setResetPassword(state, response) {
      return (state.resetPasswordResponse = response);
    },
    setActivities(state, response) {
      return (state.activities = response);
    }
  }
};
