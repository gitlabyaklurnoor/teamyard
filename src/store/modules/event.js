import firebase from "firebase";

export default {
  namespaced: true,
  state: {
    eventData: {},
  },
  getters: {
    
  },
  actions: {
    create({ commit }, eventData) {
      return new Promise(async (resolve, reject) => {
        try {
          var addMessage = firebase.functions().httpsCallable("createEvent");
          let response = await addMessage({ event: eventData });
          resolve(response);
          commit();
        } catch (error) {
          reject(error);
        }
      });
    },
  },
  mutations: {}
};
