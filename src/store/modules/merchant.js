import firebase from "firebase";

export default {
  namespaced: true,
  state: {
    bankInformations: {},
  },
  getters: {
    getBankInformations(state) {
      if (state.bankInformations) {
        const {
          content,
          totalElements,
          size,
          totalPages
        } = state.bankInformations;

        return { content, totalElements, size, totalPages };
      }
      return null;
    }
  },
  actions: {
    createCompany({ commit }, data) {
      return new Promise(async (resolve, reject) => {
        let authUserInfo = JSON.parse(
          localStorage.getItem("authenticatedUser")
        );
        let companyId = authUserInfo.companyId;
        try {
          let response = await firebase
            .firestore()
            .collection("company")
            .doc(companyId)
            .update({
              name: data.name,
              logo: data.logo,
              website: data.website,
              location: data.location,
              phoneNumber: data.phoneNumber,
              completedCompanyInfo: true
            });
          authUserInfo.completedCompanyInfo = true;
          authUserInfo.companyLogo = data.logo;
          localStorage.setItem(
            "authenticatedUser",
            JSON.stringify(authUserInfo)
          );
          resolve(response);
        } catch (error) {
          reject(error);
          console.log(error);
        }
      });
    },
  },
  mutations: {
    setBankInformations(state, bankInformations) {
      return (state.bankInformations = bankInformations);
    },
  }
};
