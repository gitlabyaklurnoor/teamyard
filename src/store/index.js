import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import merchant from './modules/merchant'
import user from './modules/user'
import event from "./modules/event";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    merchant,
    user,
    event
  },
  mutations: {
    setItems(state, { resource, items }) {
      state[resource].items = items;
    },
    setItem(state, { resource, item }) {
      state[resource].item = item;
    },
    addItemToArray(state, { item, index, resource }) {
      Vue.set(state[resource].items, index, item);
    }
  }
});