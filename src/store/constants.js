export const TOGGLE_LOADING = 'TOGGLE_LOADING';
export const RECENT_TRANSACTION_COUNT = 25;
export const CURRENCIES = {
  0: 'ALL',
  1: 'EUR'
};

export const GROUP_PRIVILAGES = [];
export const NONE_AUTH_URLS = [];

export const DEPARTMENTS = [
  {
    id: 0,
    name: 'Production',
    value: 'Production',
  },
  {
    id: 1,
    name: 'Research and Development',
    value: 'Research and Development',
  },
  {
    id: 2,
    name: 'Purchasing',
    value: 'Purchasing',
  },
  {
    id: 3,
    name: 'Marketing',
    value: 'Marketing',
  },
  {
    id: 4,
    name: 'Human Resource Management',
    value: 'Human Resource Management',
  },
  {
    id: 5,
    name: 'Accounting and Finance',
    value: 'Accounting and Finance',
  }
]