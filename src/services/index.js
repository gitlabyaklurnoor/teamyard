import axios from 'axios'
import { NONE_AUTH_URLS} from '../store/constants';

const baseURL = process.env.VUE_APP_GATEWAY_URL;

const axiosInstance = axios.create({
    timeout: 60000,
    baseURL
})

axiosInstance.interceptors.request.use(function (config) {
    let url = config.url;
    if(!url.includes('self-registration-request')){ // this code for /self-registration/self-registration-request/validateConfirmationMail/192932932 case
      if(!NONE_AUTH_URLS.includes(url)){
        let authenticatedUser = JSON.parse(localStorage.getItem("authenticatedUser"));
        let token = authenticatedUser.merchantPanelToken;
        if (token && token !== undefined) {
          config.headers.Authorization = `Bearer ${token}`
        }
      }
    }
    return config
}, function (err) {
    return Promise.reject(err)
});

axiosInstance.interceptors.response.use(
    async (res) => {
      return res;
    },
    async (error) => {
      if (error.response) {
        if (error.response.status === 401) {
            this.$store
            .dispatch("auth/logout")
            .then(data => {})
            .catch(() => {});
        }
      }
      return Promise.reject(error);
    }
  );

  export default axiosInstance