import Vue from 'vue'
import './plugins/vuetify'
import router from './router'
import store from './store'
import App from './App.vue'

import Base from './layout/wrappers/baseLayout.vue';
import Auth from './layout/wrappers/pagesLayout.vue';
import Info from './layout/wrappers/infoLayout.vue';

import BootstrapVue from "bootstrap-vue";
import VuejsDialog from 'vuejs-dialog';

// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import 'vue-select/dist/vue-select.css';
import VueTheMask from 'vue-the-mask'
import vSelect from 'vue-select'
import JsonExcel from './components/Excel/JsonExcel';
import firebase from 'firebase'; // Your web app's Firebase configuration
import rate from "vue-rate";
import "vue-rate/dist/vue-rate.css";

import x5GMaps from "x5-gmaps";

Vue.config.productionTip = false;

Vue.use(x5GMaps, {
  key: "AIzaSyAAz6AT8zPEXWe9iL3vKqCgpHv7LCjV8Bo",
  libraries: ["places"]
});

Vue.use(rate);
Vue.use(BootstrapVue);
Vue.use(VuejsDialog);
Vue.use(VueTheMask)

Vue.component('downloadExcel', JsonExcel)

Vue.component('vue-select', vSelect)
Vue.component('default-layout', Base);
Vue.component('auth-layout', Auth);
Vue.component('info-layout', Info);

var firebaseConfig = {
    apiKey: "AIzaSyDHaaGPOPrIjuBgrv8sQTeAam1W55eAq8U",
    authDomain: "teamyard-6b5a6.firebaseapp.com",
    databaseURL: "https://teamyard-6b5a6.firebaseio.com",
    projectId: "teamyard-6b5a6",
    storageBucket: "teamyard-6b5a6.appspot.com",
    messagingSenderId: "231359099299",
    appId: "1:231359099299:web:eecf12dd19ac81802c6a16",
    measurementId: "G-N0V61XN3X8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
